var qs = require('querystring');
var UserModel = require('./../lib/mongo').UserModel;
var users = {
    request: {},
    response: {},
    argument: '',
    api: ''
};



users.getAction = function () {
    var self = this;

    if (self.argument) {
        var query = UserModel.findOne({id: self.argument});
        query.exec(function (err, users) {
            if (users) {
                self.response.renderJson(JSON.stringify(users))
            } else {
                self.response.serverError(404, '404 Bad Request');
            }

        });
    } else {
        UserModel.find(function (err, users) {
            if (!err) {
                self.response.renderJson(JSON.stringify(users))
            }
        });
    }
};

users.putAction = function () {
    var user = this.request.body;
    var id = this.request.params.id;

    var persistedUser = UserModel.findOne(id);
    UserModel.save({
        id: persistedUser.id,
        name: user.name
    });
};
users.deleteAction = function () {
    var self = this;
    UserModel.findOneAndRemove(this.request.params.id)
        .exec(function (err, user) {
            if (err) {
                self.response.serverError(404, '404 Bad Request');
            } else {
                self.response.renderJson(JSON.stringify(users))
            }
        });
};
users.postAction = function () {
    var body = '';
    var self = this;
    self.request.on('data', function (data) {
        body += data;

        // Too much POST data, kill the connection!
        if (body.length > 1e6)
            self.request.connection.destroy();
    });
    self.request.on('end', function () {
        var post = qs.parse(body);
        var user = new UserModel({name: post['name']});
        user.save(function (err) {
            if (!err) {
                self.response.renderJson(JSON.stringify(user))
            } else {
                self.response.serverError(404, '404 Bad Request');
            }
        });
    });
};

exports.users = users;