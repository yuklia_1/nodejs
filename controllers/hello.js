var hello ={
    request: {},
    response:{},
    argument: '',
    api:''
};

hello.getAction = function(){
    var url = require('url');
    var urlParts = url.parse(this.request.url, true);
    var query = urlParts.query;
    return "Hello, {0}.".f(query.name);
};
exports.hello = hello;