var qs = require('querystring')
var submit ={
    request: {},
    response:{},
    argument: '',
    api:''
};

submit.postAction = function(){
    var self = this;
    var body ="";
    self.request.on('data', function (data) {
        body += data;

        // Too much POST data, kill the connection!
        if (body.length > 1e6)
            self.request.connection.destroy();
    });
    self.request.on('end', function () {
        var post = qs.parse(body);
        self.response.end('Hello '+ post.name);
    });
};

exports.submit = submit;