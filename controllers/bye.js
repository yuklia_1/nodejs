var bye ={
    request: {},
    response:{},
    argument: '',
    api:''
};

bye.getAction = function(){
    var url = require('url');
    var urlParts = url.parse(this.request.url, true);
    var query = urlParts.query;
    return "Bye {0} !=))".f(query.name);
};
exports.bye = bye;