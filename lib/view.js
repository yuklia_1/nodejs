/**
 * Created by yuklia on 08.02.15.
 */
var Mustache = require('mustache');
var view = {};
view.setContent  = function(layout, content) {
        return Mustache.to_html(layout, content);
    };

exports.view = view;