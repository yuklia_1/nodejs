var fs                      = require('fs');
var url                     = require('url');
var controller              = require('./controller');
require('./utils');
//require('./exception');

function Exception(message){
    this.name = 'ControllerNotFoundException';
    this.message = message;
}

var Dispatcher = {

    controllers: ['users','hello','bye','form','submit'], //mapping
    methodPrefix: 'Action',
    isControllerExist : function(value){
        for(var i=0;i<this.controllers.length;i++){
            if(this.controllers[i] == value){
                return true;
            }
        }
        return false;
    }


};

exports.dispatch = function(req, res) {
    var parts = req.url.split('/');
    console.log(parts);

    var api = '';
    var controller = '';
    var argument = '';
    if(parts[1] == 'api'){
        api = parts[1];
        controller = parts[2];
        argument = parts[3];
    }else if(parts[1].indexOf('?') != -1){
        controller = parts[1].substr(0, parts[1].indexOf('?'));
        console.log(controller);
    }else{
        controller = parts[1];
        argument = parts[2];
    }

    if(!Dispatcher.isControllerExist(controller)){
        res.serverError(404, 'Controller {0} not found'.f(controller));
    }
    var exp = require('./../controllers/' + controller);
    var method = req.method.toLowerCase() + Dispatcher.methodPrefix;

    if (typeof exp[controller][method] == 'function') {
        exp[controller].request = req;
        exp[controller].response = res;
        exp[controller].argument = argument;
        exp[controller].api = api;
        exp[controller][method]();

    } else {
        res.serverError(404, '404 Bad Request');
    }
};