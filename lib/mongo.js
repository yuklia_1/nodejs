/**
 * Created by yuklia on 08.02.15.
 */
var mongoose    = require('mongoose');

mongoose.connect('mongodb://localhost/nodejs');
var db = mongoose.connection;

db.on('error', function (err) {
    console.error('connection error:', err.message);
});
db.once('open', function callback () {
    console.info("Connected to DB!");
});

var UsersSchema = mongoose.Schema({
    id: Number,
    name: String
});

var UserModel = mongoose.model('Users', UsersSchema);

module.exports.UserModel = UserModel;